from nose.tools import assert_equal
from nose.tools import assert_almost_equal
import aims

def test_ints():
    x = [1,4,7,4]
    obs = aims.std(x)
    exp = 2.12
    assert_equal(obs,exp)

def test_ints():
    x = [1,4,7,4]
    obs = aims.std(x)
    exp = 2.12
    assert_almost_equal(obs,exp)

def test_ints():
    x = [1,4,7,4]
    obs = aims.std(x)
    exp = 2.12
    assert_almost_equal(obs,exp)

def test_ints():
    x=(-1,-2,-3,-4,-5)
    obs = aims.std(x)
    exp = 1.4142

def test_avg():
    x=['data/bert/audioresult-00215']
    obs = aims.avg_range(x)
    exp = 5
    assert_equal(obs,exp)

def test_avg():
    x=['data/bert/audioresult-00222']
    obs = aims.avg_range(x)
    exp = 5
    assert_equal(obs,exp)

def test_avg():
    x=['data/bert/audioresult-00470']
    obs = aims.avg_range(x)
    exp = 8 
    assert_equal(obs,exp)

